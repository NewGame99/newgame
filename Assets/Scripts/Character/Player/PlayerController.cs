﻿using System;
using System.Collections;
using UnityEngine;
using NewGame.Skill;
using NewGame.CharacterState;

public class PlayerController : MonoBehaviour {

    [SerializeField]
    private Joystick joystick;   // ジョイスティック

    private PlayerSkillEffectProcess playerSkillEffectProcess;
    private PlayerInfo playerInfo;
    private Animator animator;
    private Rigidbody objRigidbody;
    private Transform objTransform;
    private PlayerPanelInfo playerPanelInfo;

    private PlayerState state;                      // 状態
    private PlayerState requestState;               // 状態リクエスト
    private float speed = 6.0f;                     // 移動速度
    private float delayTime = 0.5f;                 // ディレイ時間
    private float rigorTime = 0.7f;                 // 硬直時間
    private Vector3 moveAmount = Vector3.zero;      // 移動量
    private Vector3 lookDirection = Vector3.zero;   // 見る方向
    private bool waitFlg = false;                   // 攻撃中はディレイ時間分待機中となる
    private bool runFlg = false;                    
    private WaitForSeconds delayTimeWait;
    private WaitForSeconds rigorTimeWait;

    private AudioSource attackAudio;
    private AudioSource runAudio;

    // 定数
    private const String run = "Run";
    private const String attack1 = "Attack1";
    private const String attack2 = "Attack2";
    private const String damage = "Damage";
    private const String death = "Death";

    public PlayerState State {
        get { return state; }
    }

    void Start() {
        playerSkillEffectProcess = GetComponent<PlayerSkillEffectProcess>();
        playerPanelInfo = GameObject.FindGameObjectWithTag("PlayerPanel").GetComponent<PlayerPanelInfo>();
        playerInfo = GetComponent<PlayerInfo>();
        animator = GetComponent<Animator>();
        objRigidbody = GetComponent<Rigidbody>();
        objTransform = gameObject.transform;

        delayTimeWait = new WaitForSeconds(delayTime);
        rigorTimeWait = new WaitForSeconds(rigorTime);

        GameObject playerAudioSource = GameObject.FindGameObjectWithTag("PlayerAudioSource");
        attackAudio = playerAudioSource.transform.Find("Attack").gameObject.GetComponent<AudioSource>();
        runAudio = playerAudioSource.transform.Find("Run").gameObject.GetComponent<AudioSource>();

        Init();
    }

    private void Init() {
        state = PlayerState.WAIT;
        requestState = PlayerState.NONE;
    }

    void FixedUpdate() {

        if (waitFlg || PlayerState.DEATH.Equals(state)) {
            return;
        }

        CheckPlayerState();

        if (state.Equals(PlayerState.MOVE)) {
            // プレイヤー移動
            objTransform.LookAt(objTransform.position + moveAmount);
            objTransform.position += moveAmount * speed * Time.fixedDeltaTime;
        }
    }

    // プレイヤーの状態チェック
    private void CheckPlayerState() {

        // リクエストがある場合
        if (!PlayerState.NONE.Equals(requestState)) {

            switch (requestState) {
                case PlayerState.WAIT:
                    ChangeState(PlayerState.WAIT);
                    break;

                case PlayerState.MOVE:
                    ChangeState(PlayerState.MOVE);
                    break;

                case PlayerState.ATTACK:
                    ChangeState(PlayerState.ATTACK);
                    break;

                case PlayerState.DAMAGE:
                    if (state.Equals(PlayerState.ATTACK) || state.Equals(PlayerState.DAMAGE)) {
                        break;
                    }
                    ChangeState(PlayerState.DAMAGE);
                    break;

                case PlayerState.DEATH:
                    ChangeState(PlayerState.DEATH);
                    break;

            }

            requestState = PlayerState.NONE;

        } else {

            // 移動するかチェック
            Boolean result = CheckMoveAmount();

            switch (state) {
                case PlayerState.WAIT:
                    if (result) {
                        ChangeState(PlayerState.MOVE);
                    }
                    break;

                case PlayerState.MOVE:
                    if (!result) {
                        ChangeState(PlayerState.WAIT);
                    }
                    break;

                case PlayerState.ATTACK:
                    if (result) {
                        ChangeState(PlayerState.MOVE);
                    } else {
                        ChangeState(PlayerState.WAIT);
                    }
                    break;
            }

        }
    }

    // 指定された状態に変更
    private void ChangeState(PlayerState targetState) {

        state = targetState;

        switch (targetState) {
            case PlayerState.WAIT:

                if (runFlg) {
                    runFlg = false;
                    runAudio.Stop();
                    animator.SetBool(run, false);
                }
                break;

            case PlayerState.MOVE:
                runFlg = true;
                runAudio.Play();
                animator.SetBool(run, true);
                break;

            case PlayerState.ATTACK:
                AttackProcess();
                break;

            case PlayerState.DAMAGE:
                StartCoroutine(DamageProcess());
                break;

            case PlayerState.DEATH:
                animator.SetTrigger(death);
                break;
        }
    }

    private IEnumerator DamageProcess() {

        objTransform.LookAt(lookDirection);

        animator.SetTrigger(damage);

        // ディレイ分待機
        yield return rigorTimeWait;

        // 移動量チェック
        if (CheckMoveAmount()) {
            requestState = PlayerState.MOVE;
        } else {
            requestState = PlayerState.WAIT;
        }
    }

    // 攻撃処理
    private void  AttackProcess() {
    
        objTransform.LookAt(lookDirection);

        animator.SetTrigger(attack1);

        EffectProcess();

        StartCoroutine(WaitProcess());
    }

    private IEnumerator WaitProcess() {
        waitFlg = true;
        yield return delayTimeWait;
        waitFlg = false;
    }

    // エフェクト処理
    private void EffectProcess() {

        EffectData effectData = playerInfo.EffectData;

        if (playerInfo.Buff) {
            playerInfo.SetBuff();
        }

        playerSkillEffectProcess.CreateEffet(effectData);
    }

    private Boolean CheckMoveAmount() {
        // 移動量を取得
        Vector2 v1 = joystick.Direction;
        moveAmount = new Vector3(v1.x, 0.0f, v1.y);

        // 0.0fより大きい ＝ 移動する
        return 0.0f < moveAmount.magnitude;
    }

    // 攻撃可能かチェック
    public Boolean CheckAttack() {
        // 攻撃中または死亡していた場合
        if (state.Equals(PlayerState.ATTACK) || state.Equals(PlayerState.DEATH)) {
            return false;
        }

        return true;
    }

    public void RequestState(PlayerState state, Vector3 direction) {
        this.lookDirection = direction;
        requestState = state;
    }

    public void RequestState(PlayerState state) {
        requestState = state;
    }

}