﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace NewGame {

    namespace CharacterState {

        // プレイヤー用
        public enum PlayerState {
            NONE,
            WAIT,
            MOVE,
            ATTACK,
            DAMAGE,
            DEATH
        }
    }
}

