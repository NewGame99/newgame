﻿using System.Collections.Generic;
using UnityEngine;
using NewGame.Damage;
using NewGame.Skill;
using NewGame.MessageFormat;

public class EnemyInfo : MonoBehaviour {

    [SerializeField]
    private Transform effceStartPoint;  // エフェクト生成位置
    [SerializeField]
    private GameObject enemyCollider;   // コライダー
    [SerializeField]
    private Transform core;             // コアオブジェクト
    [SerializeField]
    private float maxHp;                // MAXHP
    [SerializeField]
    private int exp;                    // 経験値

    private EnemyController enemyController;
    private EnemyPanelInfo enemyPanelInfo;

    private float nowHp;            // 現在HP
    private bool deathFlg = false;  // 死亡フラグ

    private List<SkillData> useSkillDataList;                       // 使用可能スキルリスト
    private EffectData selectSkillEffectData = new EffectData();    // 選択されているスキルのエフェクトデータ

    public float HpRatio {
        get { return (nowHp / maxHp) * 100.0f; }
    }
   
    public bool Death {
        get { return deathFlg; }
    }
    
    public Transform Core {
        get { return core; }
    }
        
    public EffectData EffectData {
        get { return selectSkillEffectData; }
    }
            
    public GameObject Collider {
        get { return enemyCollider; }
    }

    void Awake() {
        StatusInit();

        List<int> useSkillNoList = new List<int> { 1 };
        useSkillDataList = SkillDataManager.GetTargetSkillList(useSkillNoList, false);

        Random random = new Random();
        int result = Random.Range(0, useSkillDataList.Count);
        SetEffectData(useSkillDataList[result]);

        enemyController = GetComponent<EnemyController>();
    }
	
	void FixedUpdate() {

        if (!deathFlg && nowHp <= 0) {
            deathFlg = true;
            enemyCollider.SetActive(false);
            enemyController.RequestState();

            MessageWindow.Instance.SetText(string.Format(MessageFormat.messageFormat001, exp));
        }
    }

    // ステータス初期化
    private void StatusInit() {
        nowHp = maxHp;
    }

    // ダメージ計算
    public void DamageCalculation(float damage) {
        // ダメージ計算結果を現在HPにセット
        nowHp = DamageProcess.Process(nowHp, damage);

        // パネルの敵情報が自身だった場合
        if (EnemyPanelInfo.Instance.CheckTargetEnemy(this)) {
            SetEnemyPanelHp();
        }
    }

    // パネルのHPを更新
    public void SetEnemyPanelHp() {
        EnemyPanelInfo.Instance.SetHp(HpRatio);
    }

    private void SetEffectData(SkillData data) {

        selectSkillEffectData.skillData = data;

        // バフの場合
        if (SkillType.BUFF.Equals(data.type)) {
            // コアを開始位置とする
            selectSkillEffectData.startPoint = core;

        } else {
            selectSkillEffectData.startPoint = effceStartPoint;
        }
    }

    public void SetEffectData(Transform playerCoreObjTrans) {
        selectSkillEffectData.goalPoint = playerCoreObjTrans;
    }

    public void SetEffectData(Vector3 goalPosition) {
        selectSkillEffectData.goalPosition = goalPosition;
    }
}
