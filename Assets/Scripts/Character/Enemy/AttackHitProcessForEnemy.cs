﻿using UnityEngine;

// 攻撃が当たった場合の処理（敵用）
public class AttackHitProcessForEnemy : MonoBehaviour {

    [SerializeField]
    private GameObject parent;   // 親オブジェクト

    private EnemyInfo enemyInfo;
    private EnemyController enemyController;

    private GameObject damageText;

    public GameObject Parent {
        get { return parent; }
    }

    void Awake() {
        damageText = (GameObject)Resources.Load("Prefabs/Text/DamageText");

        enemyInfo = parent.GetComponent<EnemyInfo>();
        enemyController = parent.GetComponent<EnemyController>();
    }

    // オブジェクトに触れた場合の処理
    void OnCollisionEnter(Collision coll) {

        GameObject obj = coll.gameObject;

        if (obj.CompareTag("PlayerSkillEffect")) {

            // ターゲットオン
            enemyController.TragetOn();

            // 衝突したエフェクトの情報を取得
            SkillEffectInfo skillInfo = obj.GetComponent<SkillEffectInfo>();

            // ダメージ計算
            enemyInfo.DamageCalculation(skillInfo.Power);

            // ダメージテキスト生成
            GameObject textObj = Instantiate(damageText,
                enemyInfo.Core.position,
                Quaternion.identity);
            textObj.GetComponent<DamageText>().SetDamageText(skillInfo.Power);
        }

    }

}
