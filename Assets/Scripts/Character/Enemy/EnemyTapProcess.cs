﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EnemyTapProcess : MonoBehaviour, IPointerClickHandler {

    private EnemyInfo enemyInfo;
    private EnemyController enemyController;
    private BattleManager battleManager;

    void Start() {
        GameObject manager = GameObject.FindGameObjectWithTag("Manager");
        battleManager = manager.GetComponent<BattleManager>();
        enemyInfo = GetComponent<EnemyInfo>();
        enemyController = GetComponent<EnemyController>();
    }

    // アタッチされたオブジェクトがタップされた時の処理
    public void OnPointerClick(PointerEventData eventData) {

        if (!enemyInfo.Death) {
            // バトル処理リクエスト
            battleManager.PlayerButtleProcess(gameObject, enemyController);
        }
    }
}
