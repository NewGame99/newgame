﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using NewGame.Skill;

// 必須スクリプト
[RequireComponent(typeof(EnemyInfo))]

public class EnemyController : MonoBehaviour {

    // 敵の状態管理用
    private enum State {
        NONE,
        WAIT,
        PATROL,
        TRACKING,
        ATTACK,
        TRAGET,
        DEATH,
    }
    
    [SerializeField]
    private Boolean autoButtleSwitch = false;   // 自動戦闘スイッチ
    [SerializeField]
    private Boolean autoPatrolSwitch = false;   // 自動巡回スイッチ
    [SerializeField]
    private float lockOnRange = 10.0f;          // ロックオン範囲
    [SerializeField]
    private float trackingRange = 22.0f;        // 追尾可能範囲
    [SerializeField]
    private float attackRange = 5.0f;           // 攻撃可能範囲
    [SerializeField]    
    private float waitTime = 1.0f;              // 攻撃するまでの待機時間
    [SerializeField]
    private float attackTime = 1.29f;           // 攻撃時間

    private EnemyInfo enemyInfo;
    private SkillEffectManager effectManager;
    private PlayerInfo playerInfo;
    private Transform player;
    private Transform playerCore;
    private Transform objTransform;
    private NavMeshAgent navMeshAgent;
    private Animator animator;
    
    private State state = State.NONE;           // 状態
    private State requestState = State.NONE;    // 状態リクエスト
    private bool attackFlg = false;             // 攻撃中フラグ
    private bool waitFlg = false;               // 攻撃前の待機中フラグ
    private bool runFlg = false;                // 移動フラグ
    private WaitForSeconds waitTimeWait;
    private WaitForSeconds attackTimeWait;

    // 定数
    private const String run = "Run";
    private const String attack = "Attack";
    private const String damage = "Damage";
    private const String death = "Death";

    void Start() {
        GameObject manager = GameObject.FindGameObjectWithTag("Manager");
        enemyInfo = GetComponent<EnemyInfo>();
        effectManager = manager.GetComponent<SkillEffectManager>();

        GameObject obj = GameObject.FindGameObjectWithTag("Player");
        player = obj.transform;
        playerInfo = obj.GetComponent<PlayerInfo>();
        playerCore = playerInfo.Core;
        objTransform = gameObject.transform;
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        waitTimeWait = new WaitForSeconds(waitTime);
        attackTimeWait = new WaitForSeconds(attackTime);

        // 待機状態に初期化
        state = State.WAIT;
    }

    void FixedUpdate() {

        if (attackFlg) {
            return;
        }

        // 状態チェック
        CheckState();

        switch (state) {
            case State.TRACKING:
                // プレイヤー座標まで移動
                navMeshAgent.SetDestination(player.position);
                break;

            case State.PATROL:
                // 移動が終わった場合
                if (navMeshAgent.remainingDistance <= 0.0f) {
                    ChangeState(State.WAIT);
                }
                break;

            case State.TRAGET:
                // プレイヤー座標まで移動
                navMeshAgent.SetDestination(player.position);
                break;

            case State.ATTACK:
                if (!waitFlg) {
                    // 続けて攻撃する場合
                    StartCoroutine(AttackProcess());
                }
                break;
        }

    }

    // 状態チェック
    private void CheckState() {

        if (!State.NONE.Equals(requestState)) {
            switch (requestState) {
                case State.TRAGET:
                    if (SearchPlayer(attackRange)) {
                        ChangeState(State.ATTACK);

                    } else {
                        ChangeState(State.TRAGET);
                    }
                    break;

                case State.DEATH:
                    ChangeState(State.DEATH);
                    break;
            }

            requestState = State.NONE;

        } else {

            switch (state) {
                case State.WAIT:
                    // 自動戦闘ONの場合
                    if (autoButtleSwitch) {
                        // 攻撃範囲にプレイヤーが存在する場合　      →　攻撃状態
                        // ロックオン範囲にプレイヤーが存在する場合　→　追尾状態
                        if (SearchPlayer(attackRange)) {
                            ChangeState(State.ATTACK);
                            break;
                        } else if (SearchPlayer(lockOnRange)) {
                            ChangeState(State.TRACKING);
                            break;
                        }
                    }

                    // 自動巡回ONの場合
                    if (autoPatrolSwitch) {
                        ChangeState(State.PATROL);
                    }
                    break;

                case State.PATROL:
                    // 自動戦闘ONの場合
                    if (autoButtleSwitch) {
                        // 攻撃範囲にプレイヤーが存在する場合　      →　攻撃状態
                        // ロックオン範囲にプレイヤーが存在する場合　→　追尾状態
                        if (SearchPlayer(attackRange)) {
                            ChangeState(State.ATTACK);

                        } else if (SearchPlayer(lockOnRange)) {
                            ChangeState(State.TRACKING);

                        }
                    }
                    break;

                case State.TRACKING:
                    // 攻撃範囲にプレイヤーが存在する場合        →　攻撃状態
                    // 追尾範囲にプレイヤーが存在しない場合      →　待機状態
                    if (SearchPlayer(attackRange)) {
                        ChangeState(State.ATTACK);

                    } else if (!SearchPlayer(trackingRange)) {
                        ChangeState(State.WAIT);

                    }
                    break;

                case State.ATTACK:
                    // 攻撃範囲にプレイヤーが存在しない場合
                    if (!SearchPlayer(attackRange)) {

                        // 追尾範囲にプレイヤーが存在しない場合  →　巡回状態 or 待機状態
                        // 追尾範囲にプレイヤーが存在する場合    →　追尾状態
                        if (!SearchPlayer(trackingRange)) {

                            if (autoPatrolSwitch) {
                                ChangeState(State.PATROL);
                            } else {
                                ChangeState(State.WAIT);
                            }

                        } else {
                            ChangeState(State.TRACKING);
                        }
                    }
                    break;

                case State.TRAGET:
                    // 攻撃範囲にプレイヤーが存在しない場合
                    if (!SearchPlayer(attackRange)) {

                        // 追尾範囲にプレイヤーが存在しない場合  →　巡回状態 or 待機状態
                        // 追尾範囲にプレイヤーが存在する場合    →　追尾状態
                        if (!SearchPlayer(trackingRange)) {

                            if (autoPatrolSwitch) {
                                ChangeState(State.PATROL);
                            } else {
                                ChangeState(State.WAIT);
                            }

                        } else {
                            ChangeState(State.TRACKING);
                        }

                    } else {
                        ChangeState(State.ATTACK);
                    }
                    break;
            }
        }
    }

    // 指定された状態に変更
    private void ChangeState(State targetState) {

        switch (targetState) {
            case State.WAIT:
                navMeshAgent.ResetPath();
                CancelMotion();
                break;

            case State.PATROL:
                // ランダム座標を取得し移動
                runFlg = true;
                animator.SetBool(run, true);
                navMeshAgent.SetDestination(GetRandomPosition());
                break;

            case State.TRACKING:
                if (!runFlg) {
                    runFlg = true;
                    animator.SetBool(run, true);
                }
                break;

            case State.ATTACK:
                StartCoroutine(AttackProcess());
                break;

            case State.TRAGET:
                // プレイヤー座標まで移動
                if (!runFlg) {
                    runFlg = true;
                    animator.SetBool(run, true);
                }
                navMeshAgent.SetDestination(player.position);
                break;

            case State.DEATH:
                CancelMotion();
                navMeshAgent.ResetPath();
                animator.SetTrigger(death);
                break;
        }

        state = targetState;
    }

    // 攻撃処理
    private IEnumerator AttackProcess() {

        // 移動停止
        navMeshAgent.ResetPath();
        CancelMotion();

        // 方向転換
        objTransform.LookAt(player.position);

        // 待機
        waitFlg = true;
        yield return waitTimeWait;
        waitFlg = false;

        // 攻撃状態のままの場合
        if (state.Equals(State.ATTACK)) {

            objTransform.LookAt(player.position);

            animator.SetTrigger(attack);

            StartCoroutine(EffectProcess());

            StartCoroutine(AttackWaitProcess());

        } else {
            // 攻撃状態以外の場合（待機中に、プレイヤーが移動した場合など）
            // なにもしない
        }
    }

    private IEnumerator AttackWaitProcess() {
        attackFlg = true;
        yield return attackTimeWait;
        attackFlg = false;
    }

    // モーションをキャンセル
    private void CancelMotion() {

        if (runFlg) {
            runFlg = false;
            animator.SetBool(run, false);
        }
    }

    private IEnumerator EffectProcess() {

        // プレイヤーのコアオブジェクトを目標座標とする
        enemyInfo.SetEffectData(playerCore.position);
        enemyInfo.SetEffectData(playerCore);

        // とりあえず適当な感覚でエフェクト生成
        yield return new WaitForSeconds(0.3f);

        for (int i = 0; i < 5; i++) {
            effectManager.CreateEffect(enemyInfo.EffectData);
            yield return new WaitForSeconds(0.1f);
        }
    }

    // ランダム座標を取得
    private Vector3 GetRandomPosition() {
        // 取得範囲は、現在座標から半径10ｍまで
        Vector2 randomPosition = UnityEngine.Random.insideUnitCircle * 10;
        Vector3 position = objTransform.position + new Vector3(randomPosition.x, 0, randomPosition.y);

        return position;
    }

    //　プレイヤーが、指定した距離に存在するか検索
    private Boolean SearchPlayer(float range) {

        // プレイヤーが死亡していた場合
        if (playerInfo.Death) {
            // 以下処理を行わない
            return false;
        }

        // プレイヤーとの距離を取得
        float distance = (player.position - objTransform.position).sqrMagnitude;
        return distance <= range * range;
    }

    // プレイヤーに攻撃された時
    public void TragetOn() {
        // 攻撃中以外、またはターゲット中以外の場合
        if (!State.ATTACK.Equals(state) && !State.TRAGET.Equals(state)) {
            requestState = State.TRAGET;
        }
    }

    public void RequestState() {
        requestState = State.DEATH;
    }

    // インポートしたアセットより必要
    // TODO:とりあえず放置
    public void attackHit() {
    }
    public void AttackFinished() {
    }
    public void HeroDie() {
    }
}
