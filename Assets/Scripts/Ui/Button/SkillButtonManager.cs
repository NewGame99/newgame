﻿using NewGame.CharacterState;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SkillButtonManager : MonoBehaviour {

    private PlayerInfo playerInfo;
    private PlayerController playerController;

    private List<Image> skillIconImageList = new List<Image>();     // スキルアイコン画像
    private int selectIndex;                                        // Listのindexで選択されているスキルを管理

    // 定数
    private const float skillOffValue = 0.7f;
    private const float skillOnValue = 1.0f;

    void Start () {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        playerInfo = player.GetComponent<PlayerInfo>();
        playerController = player.GetComponent<PlayerController>();

        // 名前順でソートしてリスト化
        var skillButtonArray = GameObject.FindGameObjectsWithTag("SkillButton").OrderBy(x => x.name);

        foreach (GameObject skillIcon in skillButtonArray) {
            skillIconImageList.Add(skillIcon.GetComponent<Image>());
        }

        selectIndex = playerInfo.SkillNoListIndex;

        // 選択されているスキルアイコンをON
        // 選択/未選択をアイコンの透過率で分割
        ChangeColor(skillIconImageList[selectIndex], skillOnValue);
    }

    public void ChangeSkill(int index) {
        // 攻撃中または、すでに選択されている場合
        if (playerController.State.Equals(PlayerState.ATTACK) || selectIndex == index) {
            return;
        }

        // 元スキルアイコンをOFF
        ChangeColor(skillIconImageList[selectIndex], skillOffValue);

        // 指定スキルデータをセット
        playerInfo.ChangeSkillData(index);
        // 指定スキルアイコンをON
        ChangeColor(skillIconImageList[index], skillOnValue);

        selectIndex = index;
    }

    private void ChangeColor(Image iconImage, float value) {
        Color color = iconImage.color;
        color.a = value;
        iconImage.color = color;
    }
}
