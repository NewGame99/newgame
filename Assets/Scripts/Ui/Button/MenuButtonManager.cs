﻿using NewGame.CharacterState;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MenuButtonManager : MonoBehaviour {

    private PlayerInfo playerInfo;
    private PlayerController playerController;

    private GameObject openWindow = null;   // 現在開かれているウィンドウ

    public void OpenCloseWindow(GameObject targetWindow) {

        if (openWindow == null) {
            openWindow = targetWindow;
            targetWindow.SetActive(true);

        } else {

            if (openWindow == targetWindow) {
                // 既に、開かれているウィンドウの場合、ウィンドウを閉じる
                targetWindow.SetActive(false);
                openWindow = null;

            } else {
                // 開かれているウィンドウを閉じる
                openWindow.SetActive(false);
                openWindow = targetWindow;

                // 新しいウィンドウを開く
                targetWindow.SetActive(true);
            }
        }
    }
}
