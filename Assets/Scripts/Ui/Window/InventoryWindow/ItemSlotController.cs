﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using NewGame.Item;


public class ItemSlotController : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler {

    private GameObject dragItem;        // ドラッグアイテム
    private Image dragItemIcon;         // ドラッグアイテムのアイコン
    private RectTransform rectTransform;

    private Coroutine clickCoroutine = null;

    // 定数
    private const String itemSlotTag = "ItemSlot";
    private const String itemIconTag = "ItemIcon";
    private const String groundTag = "Ground";
    private const float clickTimeout = 0.25f;

    public ItemData ItemData {
        private get;
        set;
    }

    void Awake() {
        //dragItem = transform.Find("ItemIcon").gameObject;
        dragItemIcon = GetComponent<Image>();
        rectTransform = (RectTransform)GetComponent<RectTransform>().parent;
    }

    void OnEnable() {

    }

    public void OnBeginDrag(PointerEventData eventData) {


    }

    public void OnDrag(PointerEventData eventData) {

        // ドラッグ中はドラッグ位置にアイテムがついてくるようにする
        Vector2 localPos = Vector2.zero;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            rectTransform,
            eventData.position,
            Camera.main,
            out localPos
        );
        gameObject.transform.localPosition = localPos;
    }

    public void OnEndDrag(PointerEventData eventData) {

        GameObject hitItem = eventData.pointerCurrentRaycast.gameObject;

        // 同じスロットの場合、元の位置に戻して終了
        if (hitItem == null || hitItem == gameObject) {
            gameObject.transform.localPosition = Vector3.zero;
            return;
        }

        Image hitItemIcon = null;

        switch (hitItem.tag) {

            case itemSlotTag:

                GameObject item = hitItem.transform.Find("ItemIcon").gameObject;
                hitItemIcon = item.GetComponent<Image>();

                // ドラッグ中のアイテムを移動させる
                hitItemIcon.sprite = dragItemIcon.sprite;
                item.SetActive(true);

                dragItemIcon.sprite = null;
                gameObject.SetActive(false);

                gameObject.transform.localPosition = Vector3.zero;
                break;

            case itemIconTag:

                hitItemIcon = hitItem.GetComponent<Image>();

                // ドラッグ中のアイテムと入れ替える
                Sprite hitItemSprite = hitItemIcon.sprite;
                hitItemIcon.sprite = dragItemIcon.sprite;
                dragItemIcon.sprite = hitItemSprite;

                gameObject.transform.localPosition = Vector3.zero;
                break;

            case groundTag:
                gameObject.transform.localPosition = Vector3.zero;
                break;

            default:
                gameObject.transform.localPosition = Vector3.zero;
                break;

        }
    }

    public void OnPointerClick(PointerEventData eventData) {

        if (clickCoroutine == null) {
            clickCoroutine = StartCoroutine(ClickCoroutine());

        } else {
            StopCoroutine(clickCoroutine);
            clickCoroutine = null;

            Debug.Log("呼ばれた");
        }
    }

    private IEnumerator ClickCoroutine() {
        yield return new WaitForSeconds(clickTimeout);
        clickCoroutine = null;
    }
}