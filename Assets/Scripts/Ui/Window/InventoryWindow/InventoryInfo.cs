﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using NewGame.Item;
using UnityEngine.EventSystems;

public class InventoryInfo : MonoBehaviour {

    [SerializeField]
    private GameObject itemWindow;        // アイテムスロット（親）
    [SerializeField]
    private GameObject equipmentWindow;   // 装備スロット（親）

    private IDictionary<GameObject, Image> itemSlotMap = new Dictionary<GameObject, Image>();

    private List<int> itemNoList;

    public static InventoryInfo Instance {
        get;
        private set;
    }

    void Awake() {
        if (Instance == null) {
            Instance = this;
        }

        itemNoList = new List<int> { 1, 2, 3, 4 };
        List<ItemData> itemDataList = ItemDataManager.GetTargetItemList(itemNoList);

        // アイテムスロットを全て取得
        List<Transform> itemSlotList = new List<Transform>();
        foreach (Transform trans in itemWindow.transform) {
            itemSlotList.Add(trans);
        }

        for (int i = 0; i < itemSlotList.Count; i++) {

            Transform itemSlot = itemSlotList[i];

            GameObject item = itemSlot.Find("ItemIcon").gameObject;
            Image itemIcon = item.GetComponent<Image>();

            // アイテムリストにアイテムが存在する場合
            if (i < itemDataList.Count) {

                ItemData itemData = itemDataList[i];

                // アイテムスロットにアイテムデータをセット
                ItemSlotController itemSlotController = item.GetComponent<ItemSlotController>();
                itemSlotController.ItemData = itemData;

                // アイテムアイコンを表示
                itemIcon.sprite = itemData.itemIcon;
                item.SetActive(true);
            }

            itemSlotMap.Add(itemSlot.gameObject, itemIcon);
        }

        var equipmentSlotArray = equipmentWindow.GetComponentsInChildren<Transform>();

    }

    void Start() {


    }

    void FixedUpdate() {

        //messageText.text


    }
}
