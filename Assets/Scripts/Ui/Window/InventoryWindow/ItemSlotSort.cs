﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using NewGame.Item;


public class ItemSlotSort : MonoBehaviour {

    void Reset() {

        float offset = 10.0f;
        int column = 1;

        bool firstFlg = true;
        int slotCount = 0;

        Vector3 basePosition = Vector3.zero;
        float slotSize = 0.0f;

        foreach (Transform slot in gameObject.transform) {

            if (firstFlg) {
                basePosition = slot.localPosition;
                slotSize = slot.GetComponent<RectTransform>().sizeDelta.x;

                firstFlg = false;
                slotCount++;

            } else {
                if (slotCount % column == 0) {

                    slot.localPosition = basePosition - new Vector3(0.0f, (slotSize + offset), 0.0f);
                    basePosition = slot.localPosition;

                    slotCount = 1;

                } else {
                    slot.localPosition = basePosition + new Vector3((slotSize + offset) * slotCount, 0.0f, 0.0f);
                    slotCount++;
                }
            }
        }
    }
}