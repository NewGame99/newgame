﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageWindow : MonoBehaviour {

    [SerializeField]
    private Text messageText;

    public static MessageWindow Instance {
        get;
        private set;
    }

    void Awake() {
        if (Instance == null) {
            Instance = this;
        }
    }

    void FixedUpdate() {

        //messageText.text


    }

    public void SetText(String text) {
        messageText.text += "\r\n" + text;
    }
}
