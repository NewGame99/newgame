﻿using UnityEngine;
using UnityEngine.UI;

public class EnemyPanelInfo : MonoBehaviour {

    private Slider hpSlider;
    private EnemyInfo targetEnemyInfo = null;

    public static EnemyPanelInfo Instance {
        get;
        private set;
    }

    void Awake() {
        if (Instance == null) {
            Instance = this;
        }

        hpSlider = gameObject.transform.Find("Hp").gameObject.GetComponent<Slider>();

        gameObject.SetActive(false);
    }

    public void SetHp(float value) {
        hpSlider.value = value;
    }

    public void PanelOn(EnemyInfo enemyInfo) {
        if (!gameObject.activeSelf) {
            gameObject.SetActive(true);
        }

        targetEnemyInfo = enemyInfo;
        SetHp(targetEnemyInfo.HpRatio);
    }

    public void PanelOff() {
        if (gameObject.activeSelf) {
            gameObject.SetActive(false);
        }
    }

    public bool CheckTargetEnemy(EnemyInfo enemyInfo) {

        bool result = false;

        if (targetEnemyInfo != null && targetEnemyInfo == enemyInfo) {
            result = true;
        }

        return result;
    }
}
