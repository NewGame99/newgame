﻿using NewGame.Skill;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPanelInfo : MonoBehaviour {

    private PlayerInfo playerInfo;
    private Transform playerBuffIcon;

    private GameObject baseIcon;        // 複製するアイコン
    private Vector3 basePosition;       // アイコン表示の基準座標

    // アイコン管理
    private Dictionary<GameObject, BuffTimer> buffIconMap = new Dictionary<GameObject, BuffTimer>();

    private Slider hpSlider;

    void Awake() {
        hpSlider = gameObject.transform.Find("Hp").gameObject.GetComponent<Slider>();

        playerBuffIcon = GameObject.FindGameObjectWithTag("PlayerBuffIcon").transform;
        baseIcon = playerBuffIcon.transform.Find("Icon").gameObject;
        basePosition = baseIcon.GetComponent<RectTransform>().localPosition;

        GameObject playerObj = GameObject.FindGameObjectWithTag("Player");
        playerInfo = playerObj.GetComponent<PlayerInfo>();
    }
	
	void FixedUpdate() {

        if (buffIconMap.Count != 0) {

            List<GameObject> deleteList = new List<GameObject>();

            foreach (KeyValuePair<GameObject, BuffTimer> icon in buffIconMap) {

                // 制限時間切れの場合
                if (icon.Value.Limit) {
                    GameObject iconObj = icon.Key;
                    Destroy(iconObj);
                    deleteList.Add(iconObj);
                }
            }

            if (deleteList.Count != 0) {
                // マップから要素を削除
                foreach (GameObject icon in deleteList) {
                    //icon.GetComponent<>()
                    buffIconMap.Remove(icon);


                }

                // アイコンをソート
                IconSort();
            }
        }
    }

    public void CreateBuffIcon(SkillData skillData) {

        // アイコン生成
        GameObject icon = Instantiate(baseIcon, playerBuffIcon);
        icon.GetComponent<Image>().sprite = skillData.buffIconImage;

        int count = buffIconMap.Count;
        if (count != 0) {
            // バフが複数ある場合、座標を変更しアイコンを整列させる
            RectTransform rectTransform = icon.GetComponent<RectTransform>();
            float x = (basePosition.x + rectTransform.sizeDelta.x) * count;
            rectTransform.localPosition = basePosition + new Vector3(x, 0, 0);
        }

        BuffTimer buffTimer = icon.GetComponent<BuffTimer>();
        buffTimer.SetBuffTimeLimit(skillData);
        icon.SetActive(true);

        buffIconMap.Add(icon, buffTimer);
    }

    public void SetHp(float value) {
        hpSlider.value = value;
    }

    // アイコンをソートして表示
    private void IconSort() {
        int count = 0;
        foreach (GameObject icon in buffIconMap.Keys) {

            if (count == 0) {
                RectTransform rectTransform = icon.GetComponent<RectTransform>();
                rectTransform.localPosition = basePosition;

            } else {
                RectTransform rectTransform = icon.GetComponent<RectTransform>();
                float x = (basePosition.x + rectTransform.sizeDelta.x) * count;
                rectTransform.localPosition = basePosition + new Vector3(x, 0, 0);
            }

            count++;
        }
    }
}
