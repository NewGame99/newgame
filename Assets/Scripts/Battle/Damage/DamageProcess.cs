﻿using UnityEngine;

namespace NewGame {
    
    namespace Damage {

        public class DamageProcess : MonoBehaviour {

            // ダメージ計算
            public static float Process(float nowHp, float damage) {

                float result = nowHp - damage;

                return result;
            }
        }
    }
}
