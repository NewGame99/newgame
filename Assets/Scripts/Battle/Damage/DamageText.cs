﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageText : MonoBehaviour {

    [SerializeField]
    private GameObject text;

    private TextMesh textMesh;
    private Color color;
    private Animator animator;

    private float time = 0.7f;
    private float value = 0.8f;

    void Awake() {
        textMesh = text.GetComponent<TextMesh>();
        color = textMesh.color;
        animator = text.GetComponent<Animator>();
    }

    void FixedUpdate() {

        if (value <= 0.0f) {
            Destroy(this.gameObject);

        }

        AnimatorStateInfo info = animator.GetCurrentAnimatorStateInfo(0);

        if (time <= info.normalizedTime) {
            color.a = value;
            textMesh.color = color;

            time += 0.1f;
            value -= 0.2f;
        }
    }

    public void SetDamageText(float damage) {
        textMesh.text = damage.ToString();
    }
}