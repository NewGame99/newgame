﻿using System;
using UnityEngine;
using NewGame.Skill;
using NewGame.CharacterState;

public class BattleManager : MonoBehaviour {

    private PlayerInfo playerInfo;
    private PlayerController playerController;

    void Start() {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        playerInfo = player.GetComponent<PlayerInfo>();
        playerController = player.GetComponent<PlayerController>();
    }
    
    // プレイヤーバトル処理（敵がタップされた時の処理）
    public void PlayerButtleProcess(GameObject enemyObj, EnemyController enemyController) {

        EnemyInfo enemyInfo = enemyObj.GetComponent<EnemyInfo>();

        // パネルをON
        EnemyPanelInfo.Instance.PanelOn(enemyInfo);

        if (playerController.CheckAttack()) {

            playerInfo.SetEffectData(enemyInfo.Core.position);
            
            // 攻撃開始
            playerController.RequestState(PlayerState.ATTACK, enemyObj.transform.position);

        } else {
            //Debug.Log("MISS");
        }
    }

    // プレイヤーバトル処理（敵以外がタップされた時の処理）
    public void PlayerButtleProcess(Vector3 tapPosition) {

        if (playerController.CheckAttack()) {
            playerInfo.SetEffectData(tapPosition);

            // 攻撃開始
            playerController.RequestState(PlayerState.ATTACK, tapPosition);
        }

    }


}
