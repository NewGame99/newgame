﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NewGame {

    namespace Item {

        public struct ItemData {
            public int no;                              // アイテムNO
            public String name;                         // アイテム名
            public Sprite itemIcon;                     // アイテムアイコン
            public ItemType itemType;                   // アイテムタイプ
            public RecoveryItemData recoveryItemData;   // 回復アイテムデータ
            public WeaponItemData weaponItemData;       // 武器アイテムデータ
        }

        public enum ItemType {
            NONE,       // なし
            RECOVERY,   // 回復アイテム
            WEAPON,     // 武器アイテム
            QUEST,      // クエストアイテム
        }

        public struct RecoveryItemData {
            public int recoveryAmount;  // 回復量
            public int coolTime;        // クールタイム
        }

        public struct WeaponItemData {
            public int restrictionLevel;        // 制限レベル
            public WeaponType type;             // 武器タイプ
            public WeaponAttribute attribute;   // 武器属性
            public int powerPhysics;            // 物理攻撃力
            public int powerMagic;              // 魔法攻撃力
        }

        public enum WeaponType {
            SWORD,      // 剣
            SPEAR,      // 槍
            KNUCKLE,    // ナックル
            STAFF,      // 杖
        }

        public enum WeaponAttribute {
            WATER,  // 水属性
            FIRE,   // 火属性
            WOOD,   // 木属性
            DARK,   // 闇属性
        }

        public class ItemDataManager {

            private static List<ItemData> itemList = new List<ItemData> {
                new ItemData {
                    no = 1,
                    name = "ベルソレッタ",
                    itemType = ItemType.WEAPON,
                    itemIcon = Resources.Load<Sprite>("Texture/ItemIcon/ItemIcon_001"),
                    weaponItemData = new WeaponItemData {
                        restrictionLevel = 1,
                        type = WeaponType.SWORD,
                        attribute = WeaponAttribute.DARK,
                        powerPhysics = 5000,
                        powerMagic = 100,
                    },
                },
                new ItemData {
                    no = 2,
                    name = "ベルソレッタ",
                    itemType = ItemType.WEAPON,
                    itemIcon = Resources.Load<Sprite>("Texture/ItemIcon/ItemIcon_001"),
                    weaponItemData = new WeaponItemData {
                        restrictionLevel = 1,
                        type = WeaponType.SWORD,
                        attribute = WeaponAttribute.DARK,
                        powerPhysics = 5000,
                        powerMagic = 100,
                    },
                },
                new ItemData {
                    no = 3,
                    name = "ベルソレッタ",
                    itemType = ItemType.WEAPON,
                    itemIcon = Resources.Load<Sprite>("Texture/ItemIcon/ItemIcon_001"),
                    weaponItemData = new WeaponItemData {
                        restrictionLevel = 1,
                        type = WeaponType.SWORD,
                        attribute = WeaponAttribute.DARK,
                        powerPhysics = 5000,
                        powerMagic = 100,
                    },
                },
                new ItemData {
                    no = 4,
                    name = "ベルソレッタ",
                    itemType = ItemType.WEAPON,
                    itemIcon = Resources.Load<Sprite>("Texture/ItemIcon/ItemIcon_001"),
                    weaponItemData = new WeaponItemData {
                        restrictionLevel = 1,
                        type = WeaponType.SWORD,
                        attribute = WeaponAttribute.DARK,
                        powerPhysics = 5000,
                        powerMagic = 100,
                    },
                },


            };

            // 対象のスキルを取得
            public static List<ItemData> GetTargetItemList(List<int> noList) {

                List<ItemData> targetItemList = new List<ItemData>();

                foreach (int no in noList) {
                    foreach (ItemData itemData in itemList) {
                        if (itemData.no == no) {
                            targetItemList.Add(itemData);
                            break;
                        }
                    }
                }

                return targetItemList;
            }
        }
    }
}
