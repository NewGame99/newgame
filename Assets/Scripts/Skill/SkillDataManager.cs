﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NewGame {

    namespace Skill {

        public struct SkillData {
            public int no;                      // スキルNO
            public String name;                 // スキル名
            public SkillAttribute attribute;    // 属性
            public int power;                   // 威力
            public int accuracy;                // 命中率
            public float speed;                 // 速度（低速：10　中速：30　高速：50）
            public float range;                 // 範囲
            public SkillType type;              // タイプ
            public Shape shape;                 // 形状
            public Sprite buffIconImage;        // バフアイコンのパス
            public float buffTimeLimit;         // バフの制限時間
            public GameObject effect;           // エフェクト
        }

        public enum SkillType {
            UNIT_ATTACK,    // 単体攻撃
            RANGE_ATTACK,   // 範囲攻撃
            BUFF,           // バフ
        }

        public enum Shape {
            NONE,       // なし
            CIRCLE,     // サークル
            LINE,       // 直線
            RANDOM,     // ランダム
        }
        
        public enum SkillAttribute {
            WATER,  // 水属性
            FIRE,   // 火属性
            WOOD,   // 木属性
        }

        public class SkillDataManager {

            // TODO:データ量に応じてリスト → 配列にしたほうがよいか
            private static List<SkillData> playerSkillList = new List<SkillData> {
                new SkillData {
                    no = 1,
                    name = "ライトニング",
                    power = 500,
                    accuracy = 85,
                    speed = 30.0f,
                    range = 8.0f,
                    type = SkillType.UNIT_ATTACK,
                    shape = Shape.NONE,
                    buffIconImage = null,
                    buffTimeLimit = 0.0f,
                    effect = (GameObject)Resources.Load("Prefabs/Effect/Player/Raitoningu/Raitoningu"),
                },
                new SkillData {
                    no = 2,
                    name = "ライトニング・ザ・レイ",
                    power = 100,
                    accuracy = 85,
                    speed = 30.0f,
                    range = 8.0f,
                    type = SkillType.RANGE_ATTACK,
                    shape = Shape.CIRCLE,
                    buffIconImage = null,
                    buffTimeLimit = 0.0f,
                    effect = (GameObject)Resources.Load("Prefabs/Effect/Player/RaitoninguZaRei/RaitoninguZaRei"),
                },
                new SkillData {
                    no = 3,
                    name = "バリア",
                    power = 500,
                    accuracy = 85,
                    speed = 30.0f,
                    range = 8.0f,
                    type = SkillType.BUFF,
                    shape = Shape.NONE,
                    buffIconImage = Resources.Load<Sprite>("Texture/Buff/Skill_3"),
                    buffTimeLimit = 30.0f,
                    effect = (GameObject)Resources.Load("Prefabs/Effect/Player/Baria/Baria"),
                },
                new SkillData {
                    no = 4,
                    name = "ライトニング・ザ・レイ",
                    power = 100,
                    accuracy = 85,
                    speed = 30.0f,
                    range = 8.0f,
                    type = SkillType.RANGE_ATTACK,
                    shape = Shape.CIRCLE,
                    buffIconImage = null,
                    buffTimeLimit = 0.0f,
                    effect = (GameObject)Resources.Load("Prefabs/Effect/Player/RaitoninguZaRei/RaitoninguZaRei"),
                },
                new SkillData {
                    no = 5,
                    name = "ライトニング・ザ・レイ",
                    power = 100,
                    accuracy = 85,
                    speed = 30.0f,
                    range = 8.0f,
                    type = SkillType.RANGE_ATTACK,
                    shape = Shape.CIRCLE,
                    buffIconImage = null,
                    buffTimeLimit = 0.0f,
                    effect = (GameObject)Resources.Load("Prefabs/Effect/Player/RaitoninguZaRei/RaitoninguZaRei"),
                },

            };

            private static List<SkillData> enemySkillList = new List<SkillData> {
                new SkillData {
                    no = 1,
                    name = "フレア",
                    power = 500,
                    accuracy = 85,
                    speed = 30.0f,
                    range = 8.0f,
                    type = SkillType.UNIT_ATTACK,
                    buffIconImage = null,
                    buffTimeLimit = 0.0f,
                    effect = (GameObject)Resources.Load("Prefabs/Effect/Enemy/Furea/Furea"),
                },
                new SkillData {
                    no = 2,
                    name = "フレア",
                    power = 100,
                    accuracy = 85,
                    speed = 30.0f,
                    range = 8.0f,
                    type = SkillType.UNIT_ATTACK,
                    buffIconImage = null,
                    buffTimeLimit = 0.0f,
                    effect = (GameObject)Resources.Load("Prefabs/Effect/Enemy/Furea/Furea"),
                },
                new SkillData {
                    no = 3,
                    name = "フレア",
                    power = 500,
                    accuracy = 85,
                    speed = 30.0f,
                    range = 8.0f,
                    type = SkillType.UNIT_ATTACK,
                    buffIconImage = null,
                    buffTimeLimit = 0.0f,
                    effect = (GameObject)Resources.Load("Prefabs/Effect/Enemy/Furea/Furea"),
                },
            };

            // 対象のスキルを取得
            public static List<SkillData> GetTargetSkillList(List<int> noList, Boolean playerFlg) {

                List<SkillData> skillList = new List<SkillData>();

                List<SkillData> targetSkillList = new List<SkillData>();
                if (playerFlg) {
                    targetSkillList = playerSkillList;
                } else {
                    targetSkillList = enemySkillList;
                }

                foreach (int no in noList) {
                    foreach (SkillData skillData in targetSkillList) {
                        if (skillData.no == no) {
                            skillList.Add(skillData);
                            break;
                        }
                    }
                }

                return skillList;
            }

            public static List<SkillData> GetAllSkillList() {
                return playerSkillList;
            }
        }
    }

}
