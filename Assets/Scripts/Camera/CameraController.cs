﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField]
    private GameObject player = null;
    
    private Vector3 offset;     // プレイヤーからカメラまでの距離

    void Start () {
        // 距離を取得する
        offset = transform.position - player.transform.position;
	}
	
    void FixedUpdate() {
        // プレイヤーからオフセット分足した座標にカメラを移動する
        transform.position = player.transform.position + offset;
    }
}
