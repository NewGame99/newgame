﻿using UnityEngine;
using NewGame.Skill;
using System.Collections;

public class RaitoninguZaRei : MonoBehaviour, EffectProcess {

    private GameObject explode;

    void Awake() {
        explode = (GameObject)Resources.Load("Prefabs/Effect/Player/RaitoninguZaRei/Explode");
        StartCoroutine(AutoDestory());
    }

    public void StartProcess(EffectData effectData) {
        // 垂直に放出

        Vector3 force = new Vector3(0.0f, -1.0f, 0.0f);

        Rigidbody rigid = GetComponent<Rigidbody>();
        rigid.AddForce(force.normalized * effectData.skillData.speed);
    }

    // オブジェクトに触れた場合の処理
    void OnCollisionEnter(Collision coll) {

        Instantiate(explode,
            transform.position,
            Quaternion.identity);

        Destroy(gameObject);
    }

    private IEnumerator AutoDestory() {
        yield return new WaitForSeconds(5.0f);

        // オブジェクトが存在する場合（画面外に存在する場合）
        if (gameObject != null) {
            Destroy(gameObject);
        }
    }
}
