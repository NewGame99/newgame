﻿using UnityEngine;
using NewGame.Skill;

public class Furea : MonoBehaviour, EffectProcess {

    // 処理開始
    public void StartProcess(EffectData effectData) {

        // エフェクトを生成した座標
        Vector3 v1 = effectData.startPoint.transform.position;

        // 目標座標にランダム座標を足した座標を取得
        // とりあえず適当値
        Vector3 v2 = effectData.goalPosition
            + new Vector3(
                Random.Range(-2.0f, 1.0f),    // x座標
                Random.Range(-0.5f, 0.5f),    // y座標
                Random.Range(-2.0f, 1.0f));   // z座標

        // 生成座標と目標座標の距離を取得
        Vector3 v3 = (v2 - v1);

        // エフェクトを飛ばす
        Rigidbody rigid = GetComponent<Rigidbody>();
        rigid.AddForce(v3.normalized * effectData.skillData.speed);

    }

    // オブジェクトに触れた場合の処理
    void OnCollisionEnter(Collision coll) {

        Instantiate((GameObject)Resources.Load("Prefabs/Effect/Enemy/Furea/Explode"),
            transform.position,
            Quaternion.identity);

        Destroy(gameObject);
    }
}
