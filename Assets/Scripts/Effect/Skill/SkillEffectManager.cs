﻿using System;
using UnityEngine;

namespace NewGame {

    namespace Skill {

        // エフェクト生成に必要なデータ
        public struct EffectData {
            public Transform startPoint;        // 開始位置
            public Transform goalPoint;         // 目標位置
            public Vector3 goalPosition;        // 目標座標
            public SkillData skillData;         // スキルデータ
        }

        public class SkillEffectManager : MonoBehaviour {

            // エフェクトを生成
            public void CreateEffect(EffectData effectData) {

                GameObject effect = Instantiate(effectData.skillData.effect,
                    effectData.startPoint.position, 
                    Quaternion.identity);

                if (!SkillType.BUFF.Equals(effectData.skillData.type)) {
                    effect.GetComponent<SkillEffectInfo>().SetEffectData(effectData.skillData);
                    effect.GetComponent<EffectProcess>().StartProcess(effectData);
                }
            }
        }
    }
}
