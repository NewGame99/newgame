﻿using UnityEngine;
using NewGame.Skill;

public class SkillEffectInfo : MonoBehaviour {

    // スキル情報
    private float power = 0.0f;     // 威力
    //private float power = 0.0f;     // 属性？

    public float Power {
        get { return power; }
    }

    void Start() {

    }
	
	void Update() {
    }

    public void SetEffectData(SkillData skillData) {
        power = skillData.power;
    }
}
