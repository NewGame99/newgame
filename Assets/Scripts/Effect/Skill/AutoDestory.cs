﻿using UnityEngine;

public class AutoDestory : MonoBehaviour {

    private ParticleSystem particle;

    void Start() {
        particle = GetComponent<ParticleSystem>();
    }

    void Update() {

        if (!particle.IsAlive()) {
            Destroy(this.gameObject);
        }

    }

}
