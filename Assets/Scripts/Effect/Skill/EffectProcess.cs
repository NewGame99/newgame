﻿using NewGame.Skill;

public interface EffectProcess {

    void StartProcess(EffectData effectData);
}
