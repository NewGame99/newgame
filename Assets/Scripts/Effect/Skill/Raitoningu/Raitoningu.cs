﻿using UnityEngine;
using NewGame.Skill;
using System.Collections;

public class Raitoningu : MonoBehaviour, EffectProcess {

    private GameObject explode;

    void Awake() {
        explode = (GameObject)Resources.Load("Prefabs/Effect/Player/Raitoningu/Explode");
        StartCoroutine(AutoDestory());
    }

    public void StartProcess(EffectData effectData) {
        // 目標座標に向かって放出

        Vector3 position = gameObject.transform.position;
        Vector3 goalPosition = effectData.goalPosition;
        Vector3 force = (goalPosition - position);

        Rigidbody rigid = GetComponent<Rigidbody>();
        rigid.AddForce(force.normalized * effectData.skillData.speed);
    }

    // オブジェクトに触れた場合の処理
    void OnCollisionEnter(Collision coll) {

        Instantiate(explode,
            transform.position,
            Quaternion.identity);

        Destroy(gameObject);
    }

    private IEnumerator AutoDestory() {
        yield return new WaitForSeconds(5.0f);

        // オブジェクトが存在する場合（画面外に存在する場合など）
        if (gameObject != null) {
            Destroy(gameObject);
        }
    }
}
