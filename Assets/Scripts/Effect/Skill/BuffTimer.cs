﻿using NewGame.Skill;
using System.Collections;
using UnityEngine;

public class BuffTimer : MonoBehaviour {

    private string buffName;
    private float timeLimit;        // 制限時間
    private bool limitFlg = false;  // 時間切れ管理

    public bool Limit {
        get { return limitFlg; }
    }

    void OnEnable() {
        StartCoroutine(StartTimer());
    }

    private IEnumerator StartTimer() {
        yield return new WaitForSeconds(timeLimit);
        limitFlg = true;
    }

    public void SetBuffTimeLimit(SkillData skillData) {
        timeLimit = skillData.buffTimeLimit;
        buffName = skillData.name;
    }
}
